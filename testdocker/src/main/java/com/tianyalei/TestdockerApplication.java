package com.tianyalei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class TestdockerApplication {
	public static int count = 0;

	@RequestMapping("")
	public String hello(){
		count++;
		return "当前count为："  + count;
	}

	public static void main(String[] args) {
		SpringApplication.run(TestdockerApplication.class, args);
	}
}
