import java.util.concurrent.CountDownLatch;

/**
 * Created by wuwf on 17/7/18.
 * 模拟N个线程同时启动
 */
public class TestManyThread {
    private CountDownLatch countDownLatch = new CountDownLatch(200);

    public static void main(String[] args) {
        new TestManyThread().begin();
    }

    public void begin() {
        for (int i = 0; i < 200; i++) {
            new Thread(new UserThread()).start();
            countDownLatch.countDown();
        }

        try {
            Thread.sleep(2000);
            System.out.println("线程并发");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private class UserThread implements Runnable {

        @Override
        public void run() {
            try {
                //等待所有线程
                countDownLatch.await();

                //TODO 在这里做客户端请求，譬如访问数据库之类的操作

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
