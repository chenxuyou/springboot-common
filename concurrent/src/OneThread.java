public class OneThread extends Thread {
    private volatile boolean running = true;


    @Override
    public void run() {
        System.out.println("进入run方法");
        while (running) {

        }
        System.out.println("线程执行完毕");
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    public boolean isRunning() {
        return running;
    }
}
