import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;


public class VolatileTest {
    private static CyclicBarrier barrier = new CyclicBarrier(100);
    private static volatile int count;

    public static void main(String[] args) {
        MyThread[] mythreadArray = new MyThread[1000];
        for (int i = 0; i < 1000; i++) {
            mythreadArray[i] = new MyThread();
            mythreadArray[i].start();
        }

    }

    static class MyThread extends Thread {
        private void addCount() {
            for (int i = 0; i < 100; i++) {
                count++;
            }
            System.out.println("count=" + count);
        }

        @Override
        public void run() {
            try {
                barrier.await();

                addCount();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            }

        }
    }
}