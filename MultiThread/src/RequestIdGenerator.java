public class RequestIdGenerator {
    private final static RequestIdGenerator INSTANCE = new RequestIdGenerator();

    private final static short LIMIT = 999;

    private short sequence = -1;

    private RequestIdGenerator() {}

    public short nextSequence() {
        if (sequence>= LIMIT) {
            sequence = 0;
        } else {
            sequence++;
        }
        return sequence;
    }

    public static RequestIdGenerator getInstance() {
        return INSTANCE;
    }
}
