package com.tianyalei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 我们需要根据IP去限制用户单位时间的访问次数，防止刷手机验证码,屏蔽注册机等
 */
@SpringBootApplication
public class IplimitApplication {

	public static void main(String[] args) {
		SpringApplication.run(IplimitApplication.class, args);
	}
}
