package com.tianyalei.controller;

import com.tianyalei.annotation.RequestLimit;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by admin on 17/7/6.
 */
@RestController
public class IndexController {
    @RequestLimit(count = 4)
    @GetMapping("/index")
    public Object index() {
        return 1;
    }
}
