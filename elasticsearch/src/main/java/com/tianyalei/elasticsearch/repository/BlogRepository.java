package com.tianyalei.elasticsearch.repository;

import com.tianyalei.elasticsearch.model.Blog;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface BlogRepository extends ElasticsearchRepository<Blog, Integer> {

}
