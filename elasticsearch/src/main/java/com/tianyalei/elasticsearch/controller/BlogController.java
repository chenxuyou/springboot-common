package com.tianyalei.elasticsearch.controller;

import com.tianyalei.elasticsearch.model.Blog;
import com.tianyalei.elasticsearch.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class BlogController {
    @Autowired
    private BlogRepository blogRepository;

    @GetMapping("/addBlog")
    public Object add() {
        for (int i = 0; i < 50; i++) {
            Blog blog = new Blog();
            blog.setId(i);
            blog.setName("name" + i);
            blog.setCreateTime(new Date());
            blogRepository.save(blog);
        }
        return "add";
    }
}
