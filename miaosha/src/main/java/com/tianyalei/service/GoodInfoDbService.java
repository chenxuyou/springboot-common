package com.tianyalei.service;

import com.tianyalei.model.GoodInfo;
import com.tianyalei.repository.GoodInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by wuwf on 17/7/5.
 */
@Service("db")
public class GoodInfoDbService implements GoodInfoService {

    @Autowired
    private GoodInfoRepository goodInfoRepository;

    @Transactional
    public int update(String code, int count) {
        return goodInfoRepository.updateAmount(code, count);
    }

    public void add(GoodInfo goodInfo) {
        goodInfoRepository.save(goodInfo);
    }

    public void delete(GoodInfo goodInfo) {
        goodInfoRepository.deleteAll();
    }

}
