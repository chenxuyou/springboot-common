package com.tianyalei.service;

import com.tianyalei.model.GoodInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by wuwf on 17/7/5.
 */
@Service("redis")
public class GoodInfoRedisService implements GoodInfoService {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public void add(GoodInfo goodInfo) {
        redisTemplate.opsForValue().set(goodInfo.getCode(), goodInfo.getAmount() + "");
    }

    @Override
    public void delete(GoodInfo goodInfo) {
        redisTemplate.delete(goodInfo.getCode());
    }

    @Override
    public int update(String code, int count) {
        if (redisTemplate.opsForValue().increment(code, -count) < 0) {
            return -1;
        }
        return 1;
    }
}
