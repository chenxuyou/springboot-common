package com.tianyalei.service;

import com.tianyalei.model.GoodInfo;

/**
 * Created by wuwf on 17/7/5.
 */
public interface GoodInfoService {
    void add(GoodInfo goodInfo);

    void delete(GoodInfo goodInfo);

    int update(String code, int count);
}
