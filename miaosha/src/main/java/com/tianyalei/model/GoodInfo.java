package com.tianyalei.model;

import javax.persistence.*;

/**
 * Created by wuwf on 17/7/5.
 */
@Entity
public class GoodInfo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    //数量
    private int amount;
    //商品编码
    @Column(unique = true)
    private String code;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
