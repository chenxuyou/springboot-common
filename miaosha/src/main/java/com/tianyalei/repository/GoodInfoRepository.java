package com.tianyalei.repository;

import com.tianyalei.model.GoodInfo;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by admin on 17/7/5.
 */
public interface GoodInfoRepository extends CrudRepository<GoodInfo, Integer> {
    @Query("update GoodInfo set amount = amount - ?2 where code = ?1 and amount - ?2 >= 0")
    @Modifying
    int updateAmount(String code, int count);
}
